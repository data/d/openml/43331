# OpenML dataset: Austin-Weather

https://www.openml.org/d/43331

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

ContextThis dataset is meant to complement the Austin Bikesharing Dataset.ContentContains the  Date YYYYMMDD  TempHighF High temperature, in Fahrenheit  TempAvgF Average temperature, in Fahrenheit  TempLowF Low temperature, in Fahrenheit  DewPointHighF High dew point, in Fahrenheit  DewPointAvgF Average dew point, in Fahrenheit  DewPointLowF Low dew point, in Fahrenheit  HumidityHighPercent High humidity, as a percentage  HumidityAvgPercent Average humidity, as a percentage  HumidityLowPercent Low humidity, as a percentage  SeaLevelPressureHighInches High sea level pressure, in inches  SeaLevelPressureAvgInches Average sea level pressure, in inches  SeaLevelPressureLowInches Low sea level pressure, in inches  VisibilityHighMiles High visibility, in miles  VisibilityAvgMiles Average visibility, in miles  VisibilityLowMiles Low visibility, in miles  WindHighMPH High wind speed, in miles per hour  WindAvgMPH Average wind speed, in miles per hour  WindGustMPH Highest wind speed gust, in miles per hour  PrecipitationSumInches Total precipitation, in inches  T if Trace  Events Adverse weather events.    if None  This dataset contains data for every date from 20131221 to 20170731.AcknowledgementsThis dataset was obtained from WeatherUnderground.com, at the Austin KATT station.InspirationCan we use this dataset to explain some of the variation in the Austin Bikesharing Dataset

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43331) of an [OpenML dataset](https://www.openml.org/d/43331). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43331/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43331/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43331/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

